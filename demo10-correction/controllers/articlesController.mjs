// Récupère le modèle Article
import Article from '../models/article.mjs';
import Category from '../models/category.mjs';

// Utilise la méthode find() afin de récupérer tous les articles
// Retourne un Promesse
export async function getArticles(req, res, next) {
  try {
    const articles = await Article.find();
    res.render('index', {
      articles: articles,
      pageTitle: 'Accueil'
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

// Récupère un article grâce à son id
export async function getArticle(req, res, next) {
  try {
    const articleId = req.params.articleId;
    const article = await Article.findById(articleId).populate("categoryId");
    res.render('article', {
      article: article,
      pageTitle: 'Accueil'
    });
  } catch (err) {
    next(err);
  }
};

// Affichage du formulaire d'ajout d'article
export async function getAddArticle(req, res, next) {
  try {
    const categories = await Category.find();
    res.render('add-article', {
      pageTitle: 'Ajouter un article',
      categories: categories
    });
  } catch (err) {
    console.log('err', err);
    next(err);
  }
};

// Affichage du formulaire d'édition d'article
export async function getEditArticle(req, res, next) {
  try {
    const articleId = req.params.articleId;
    const article = await Article.findById(articleId);
    console.log('article', article);
    res.render('edit-article', {
      article: article,
      pageTitle: 'Modifier un article'
    });
  } catch (err) {
    console.log('err', err);
    next(err);

  }
};

// Enregistre un article dans la bd
export async function createArticle(req, res, next) {
  try {
    const { title, content, category } = req.body;

    // Crée un nouvel article avec les informations du formulaire
    const article = new Article({
      title: title,
      content: content,
      categoryId: category
    });
    // Enregistre l'article dans la base de données
    // Utilisation de la méthode save() qui retourne une promesse
    const result = await article.save();
    res.redirect('/');
  } catch (err) {
    console.log('err', err);
    next(err);

  }
};

// Enregistre un article modifié dans la bd
export async function updateArticle(req, res, next) {
  try {
    const articleId = req.body.articleId;
    const title = req.body.title;
    const content = req.body.content;

    // Retrouve l'article et l'enregistre dans la base de données
    // Utilisation de la méthode save() qui retourne une promesse
    const article = await Article.findById(articleId);
    article.title = title;
    article.content = content;
    const result = await article.save();
    // exemple de réponse au format JSON
    res.status(200).json({ message: 'Article mis à jour !', article: result });
    // res.redirect('/');
  } catch (err) {
    console.log('err', err);
    next(err);

  }
};

// Suprime un article grâce à son id
export async function deleteArticle(req, res, next) {
  try {
    const articleId = req.params.articleId;
    await Article.findByIdAndDelete(articleId);
    console.log('article supprimé');
    res.redirect('/');
  } catch (err) {
    next(err);
  }
};

// Affichage du formulaire d'ajout de catégorie
export async function getAddCategory(req, res, next) {
  res.render('add-category', {
    pageTitle: 'Ajouter une catégorie'
  });
};



// createCategory
export async function createCategory(req, res, next) {
  try {
    const name = req.body.name;

    // Crée une nouvelle catégorie avec les informations du formulaire
    const category = new Category({
      name: name,
    });

    // Enregistre l'article dans la base de données
    // Utilisation de la méthode save() qui retourne une promesse
    const result = await category.save();
    res.redirect('/');
  } catch (err) {
    console.log('err', err);
    next(err);

  }
}