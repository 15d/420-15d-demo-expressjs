import express from 'express';
import path from 'path';
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import mongoose from 'mongoose';
import dotenv from 'dotenv';

import * as errorController from './controllers/error.mjs';
import articleRoutes from './routes/article.mjs';

dotenv.config();

const app = express();
const port = process.env.PORT ?? 3000;


const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// // Configuration pour template EJS
app.set('view engine', 'ejs');
// Déclarer le dossier views qui contient les templates
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));

// Déclaration d'un parser pour analyser "le corps (body)" d'une 'requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));

// Utilisation des routes en tant que middleware
app.use(articleRoutes);

// Gestion erreur 404, ce middleware doit être le dernier
app.use(errorController.get404);


mongoose
  .connect(process.env.MONGODB)
  .then(result => {
    app.listen(port, () => {
      console.log(`Le serveur écoute sur http://localhost:${port}`);
    });
  })
  .catch(err => console.log(err));


