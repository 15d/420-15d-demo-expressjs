"use strict";

import express from 'express';

import {
  getArticles,
  getArticle,
  getAddArticle,
  createArticle,
  getEditArticle,
  updateArticle,
  deleteArticle,
  getAddCategory,
  createCategory
} from '../controllers/articlesController.mjs';

const router = express.Router();

// / => GET
router.get('/', getArticles);

// /article/`articleId => GET
router.get('/article/:articleId', getArticle);

// /article/add-article => GET
router.get('/add-article', getAddArticle);

// /article/add-article => POST
router.post('/add-article', createArticle);

// /article/:articleId/edit => GET
router.get('/:articleId/edit', getEditArticle);

// /article/edit-article => POST
router.post('/edit-article', updateArticle);

// /article/delete/:articleId => GET
router.get('/delete/:articleId', deleteArticle);

// /article/add-category => GET
router.get('/add-category', getAddCategory);

// /article/add-category => POST
router.post('/add-category', createCategory);


export default router;