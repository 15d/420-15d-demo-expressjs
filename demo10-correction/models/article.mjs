import { Schema, model } from 'mongoose';

const articleSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: true
    },
    categoryId: [
      {
        // Permet de référencer d'autres documents, provenant
        // d'autres collections
        type: Schema.Types.ObjectId,
        ref: 'Category'
      }
    ]
  },
  { timestamps: true }
);

const Article = model('Article', articleSchema);
export default Article;