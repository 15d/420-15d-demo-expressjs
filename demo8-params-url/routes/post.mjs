"use strict";

import express from 'express'
import {
  getPosts,
  getPost
} from '../controllers/postsController.mjs';

const router = express.Router();

router.get('/', getPosts);
router.get('/post/:postId', getPost);

export default router;


