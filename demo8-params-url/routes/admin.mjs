"use strict";

import express from 'express'
import {
  getAddPost,
  createPost
} from '../controllers/adminController.mjs';

const router = express.Router();


// /admin/add-post => GET
router.get('/add-post', getAddPost);

// /admin/add-post => POST
router.post('/add-post', createPost);

// Export des routes pour utilisation dans app.js
export default router;


