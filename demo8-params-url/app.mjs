import express from 'express';
import path from 'path';
import { dirname } from 'path';
import { fileURLToPath } from 'url';
// Importe les routes
import adminRoutes from './routes/admin.mjs';
import postRoutes from './routes/post.mjs';
import { get404 } from './controllers/error.mjs';

const app = express();
const port = 3001;

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Configuration pour template EJS
app.set('view engine', 'ejs');
// Déclarer le dossier views qui contient les templates
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));



// Déclaration d'un parser pour analyser "le corps (body)" d'une 'requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));

// Utilisation des routes en tant que middleware
app.use('/admin', adminRoutes);
app.use(postRoutes);

app.use(get404);

app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});

