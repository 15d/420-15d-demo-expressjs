
// N'ayant pas encore de base de données, on défini quelques titre d'articles
const posts = [{id: 1, title: 'Un article'}, {id: 2, title: 'Un 2ème article'}];
export { posts };

export function getAddPost(req, res) {
  res.render('add-post', {
    pageTitle: 'Ajouter un article'
  });
};

export function createPost(req, res) {
    console.log(req.body);
    // ajout d'un id pour chaque article
    req.body.id = posts.length + 1;
    // Ajout du titre dans posts
    posts.push(req.body);
    res.redirect('/');
}
