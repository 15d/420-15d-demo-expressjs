import { posts } from './adminController.mjs';

export function getPosts(req, res) {
  res.render('index', {
    posts: posts,
    pageTitle: 'Accueil'
  });
};

export function getPost(req, res) {
  console.log('req.params', req.params);
  console.log('req.query', req.query);
  const postId = req.params.postId;
  res.render('post', {
    post: posts.find(post => post.id == postId),
    pageTitle: posts.find(post => post.id == postId).title
  });
};

