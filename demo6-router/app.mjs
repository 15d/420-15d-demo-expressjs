"use strict";

import path from 'path';
import express from 'express'
import { dirname } from 'path';
import { fileURLToPath } from 'url';
// Importe les routes
import adminRoutes from './routes/admin.mjs';
import postRoutes from './routes/post.mjs';

const app = express();
const port = 3001;

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Configuration pour template EJS
app.set('view engine', 'ejs');
// Déclarer le dossier views qui contient les templates
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));



// Déclaration d'un parser pour analyser "le corps (body)" d'une requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));

// Utilisation des routes en tant que middleware
// route /admin
app.use('/admin', adminRoutes);
// route /
app.use(postRoutes);

app.use((req, res, next) => {
  res.status(404).render('404', { pageTitle: 'Page introuvable !' });
});

app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});

