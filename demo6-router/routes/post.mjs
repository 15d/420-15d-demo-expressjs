"use strict";

import express from 'express'

// Importe les articles
import { posts } from './admin.mjs';

const router = express.Router();

router.get('/', (req, res, next) => {
  res.render('index', {
    posts: posts,
    pageTitle: 'Accueil'
  });
});

export default router;


