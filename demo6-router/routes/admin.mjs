"use strict";

import express from 'express'

const router = express.Router();

// N'ayant pas encore de base de données, on défini quelques titre d'articles
const posts = [{title: 'Un article'}, {title: 'Un 2ème article'}];

// /admin/add-post => GET
router.get('/add-post', (req, res, next) => {
  res.render('add-post', {
    pageTitle: 'Ajouter un article'
  });
});

// /admin/add-post => POST
router.post('/add-post', (req, res, next) => {
  console.log(req.body);
    // Ajout du titre dans posts
    posts.push(req.body);
    res.redirect('/');
});

// Export des articles pour utilisation dans post.mjs
export { posts };

export default router;

