"use strict";

import express from 'express'
const app = express();
const port = 3001;


const debutHtml = `<html lang="fr">
<head>
<title>Blogue</title>
</head>
<nav style="background-color:black;">
  <h1 style="text-align:center; color:white">Mon super blogue</h1>
</nav>
<body>`;

const finHtml = `</body>
</html>`;

// Déclaration d'un parser pour analyser "le corps (body)" d'une requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));



// use est exécuté quelque soit la méthode (GET, POST, PUT...)
app.use("/", (req, res, next) => {
	console.log(req.url);
	console.log("\n\nPremier middleware!");
	res.setHeader("Content-Type", "text/html; charset=utf-8");
	res.write(debutHtml);
	next();
});

// get permet de répondre seulement aux requêtes en GET à l.url spécifiée
app.get("/articles", (req, res, next) => {
	console.log("Middleware de la liste des articles");
	res.write("<h2>Liste des articles</h2>");
	next();
});

app.get('/articles/new', (req, res, next) => {
  res.write('<form action="" method="POST"><input type="text" name="title"><button type="submit">Ajouter</button></form>');
});

app.post('/articles/new', (req, res, next) => {
    // req.body permet de retrouver les données postées par le formulaire
    // Cela est possible grâce au middleware déclaré plus haut (express.urlencoded)
    console.log(req.body);
    res.write(`<p>Titre de l'article: ${req.body.title}</p>`); // récupère le champ qui à le name="title"
});

app.use("/", (req, res) => {
	console.log("Dernier middleware!");
	res.write(finHtml);
	res.end();
});


app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});
