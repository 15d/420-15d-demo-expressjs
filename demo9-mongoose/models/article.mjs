import { Schema, model } from 'mongoose';

const articleSchema = new Schema(
  {
    titre: {
      type: String,
      required: [true, "Le titre est requis"]
    },
    contenu: {
      type: String,
      required: [true, "Le contenu est requis"]
    }
  },
  { timestamps: true }
);

const Article = model('Article', articleSchema);
export default Article;