
// Récupère le modèle Article
import Article from '../models/article.mjs';

// Utilise la méthode find() afin de récupérer tous les articles
// Retourne un Promesse
export async function getArticles(req, res, next) {
  try {
    const articles = await Article.find();
    console.log('articles', articles);
    res.render('index', {
      articles: articles,
      pageTitle: 'Accueil'
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

// Récupère un article grâce à son id
export async function getArticle(req, res, next) {
  console.log('req.params', req.params);
  console.log('req.query', req.query);
  const articleId = req.params.articleId;
  try {
    const article = await Article.findById(articleId);
    res.render('article', {
      article: article,
      pageTitle: 'Accueil'
    });
  } catch (error) {
    next(error);
  }
};


// Affichage du formulaire d'ajout d'article
export async function getAddArticle(req, res, next) {
  res.render('add_article', {
    pageTitle: 'Ajouter un article',
    errors: []
  });
};

// Enregistre un article dans la bd
export async function createArticle(req, res, next) {
  const { titre, contenu } = req.body;

  // Crée un nouvel article avec les informations du formulaire
  const article = new Article({
    titre: titre,
    contenu: contenu
  });

  try {
    // Enregistre l'article dans la base de données
    await article.save();
    res.redirect('/');
  } catch (error) {
     // Gestion spécifique des erreurs de validation
    if (error.name === 'ValidationError') {
      const errors = Object.values(error.errors).map(err => err.message);
      console.log('Erreurs de validation :', errors);

      // Renvoyer les erreurs au formulaire
      const pageTitle = 'Ajouter un article';
      return res.render('add_article', { errors, titre, contenu, pageTitle });
    } else {
      // Gestion des autres types d'erreurs
      console.error('Erreur inattendue :', error);
      next(error);
    }
  }
};

// Affichage du formulaire d'édition d'article
export async function getEditArticle(req, res, next) {
  const articleId = req.params.articleId;
  try {
    const article = await Article.findById(articleId);
    console.log('article', article);
    res.render('edit_article', {
      article: article,
      pageTitle: 'Modifier un article'
    });
  } catch (error) {
    console.log('error', error);
    next(error);
  }
};

// Enregistre un article modifié dans la bd
export async function updateArticle(req, res, next) {
  const articleId = req.body.articleId;
  const titre = req.body.titre;
  const contenu = req.body.contenu;

  try {
    const article = await Article.findById(articleId);
    if (!article) {
      res.status(404).render('404', { pageTitle: 'Page introuvable !' });
    } else {
      article.titre = titre;
      article.contenu = contenu;
      await article.save();
      res.redirect('/');
    }
  } catch (error) {
    console.log('error', error);
    next(error);
  }
};


// Suprime un article grâce à son id
export async function deleteArticle(req, res, next) {
  const articleId = req.params.articleId;
  try {
    await Article.findByIdAndDelete(articleId);
    console.log('article supprimé');
    res.redirect('/');
  } catch (error) {
    console.log('error', error);
    next(error);
  }
};


