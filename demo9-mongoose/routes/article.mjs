"use strict";

import express from 'express'

import * as articlesController from '../controllers/articlesController.mjs';

const router = express.Router();

router.get('/', articlesController.getArticles);
router.get('/article/:articleId', articlesController.getArticle);

// /articles/add-article => GET
router.get('/add-article', articlesController.getAddArticle);

// /articles/add-article => POST
router.post('/add-article', articlesController.createArticle);

// /articles/:articleId/edit => GET
router.get("/articles/:articleId/edit", articlesController.getEditArticle);

// /articles/:articleId => POST
router.post("/articles/edit", articlesController.updateArticle);

// /articles/:postId/delete => GET
// Attention, ce n'est pas une bonne pratique d'utiliser GET pour supprimer des données
router.get("/articles/:articleId/delete", articlesController.deleteArticle);

export default router;

