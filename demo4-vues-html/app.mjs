"use strict";

import path from 'path';
import express from 'express'
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const app = express();
const port = 3001;

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Déclaration d'un parser pour analyser "le corps (body)" d'une 'requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));

app.get('/article/new', (req, res, next) => {
  res.setHeader("Content-Type", "text/html; charset=utf-8");
  res.sendFile(path.join(__dirname, './', 'views', 'add-post.html'));
});

app.post('/article/new', (req, res, next) => {
    console.log(req.body);
    res.redirect('/');
});

app.get('/', (req, res, next) => {
  res.setHeader("Content-Type", "text/html; charset=utf-8");
  res.sendFile(path.join(__dirname, './', 'views', 'index.html'));
});

app.use((req, res, next) => {
  res.setHeader("Content-Type", "text/html; charset=utf-8");
  res.status(404).send('<h1>Page introuvable !</h1>');
});

app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});