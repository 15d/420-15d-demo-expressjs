import { Schema, model } from 'mongoose';

const articleSchema = new Schema(
  {
    title: {
      type: String,
      required: [true, "le titre est requis"]
    },
    content: {
      type: String,
      required: [true, "le contenu est requis"]
    }
  },
  { timestamps: true }
);

const Article = model('Article', articleSchema);
export default Article;