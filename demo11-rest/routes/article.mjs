import express from 'express'
import {
  getArticles,
  getArticle,
  createArticle,
  deleteArticle,
  updateArticle
} from '../controllers/articlesController.mjs';
const router = express.Router();

// /posts => GET
router.get('/posts', getArticles);

// /post/postId => GET
router.get('/post/:postId', getArticle);

// /post => POST
router.post('/post/', createArticle);

// /post/postId => DELETE
router.delete('/post/:postId', deleteArticle);

// /post/postId => PUT
router.put('/post/:postId', updateArticle);

export default router;

