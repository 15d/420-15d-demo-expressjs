"use strict";

import express from 'express'
import mongoose from 'mongoose';

import {
  get404,
  getErrors
} from './controllers/errorController.mjs';

// Importe les routes
import articleRoutes from './routes/article.mjs';

const app = express();
const port = 3000;


// Gestion des CORS 
app.use((req, res, next) => {
  // Définit les domaines qui sont autorisés à accéder aux ressources du serveur
  // Autorise l'accès à toutes les ressources depuis n'importe quel domaine
  res.setHeader('Access-Control-Allow-Origin', '*');
  // OU 
  // Autorise l'accès à toutes les ressources depuis le domaine https://cdpn.io
  // res.setHeader('Access-Control-Allow-Origin', 'https://cdpn.io');

  // Spécifie les méthodes HTTP qui sont autorisées pour accéder aux ressources
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );

  // Définit les en-têtes HTTP qui peuvent être utilisés lors de la demande
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  // Passe au middleware suivant
  next();
});


// parse application/json
// Permet de parser le corps des requêtes (body) en JSON
app.use(express.json());

// Utilisation des routes en tant que middleware
app.use(articleRoutes);

// Gestion erreur 404, ce middleware doit être le dernier
app.use(get404);

// Gestion des erreurs
app.use(getErrors);

mongoose
  .connect('mongodb://127.0.0.1:27017/blogue')
  .then(() => {
    app.listen(port, () => {
      console.log(`Le serveur écoute sur http://localhost:${port}`);
    });
  })
  .catch(err => console.log(err));

