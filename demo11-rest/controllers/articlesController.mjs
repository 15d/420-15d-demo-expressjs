// Récupère le modèle Post
import Article from '../models/article.mjs';

export async function getArticles(req, res, next) {
  try {
    const posts = await Article.find();
    res.json(posts);
  } catch (err) {
    next(err);
  }
}

// Récupère un article grâce à son id
export async function getArticle(req, res, next) {
  const postId = req.params.postId;
  try {
    const post = await Article.findById(postId);
    if (!post) {
      const error = new Error('L\'article n\'existe pas.');
      error.statusCode = 404;
      throw error;
    }
    res.status(200).json(post);
  } catch (err) {
    next(err);
  }
}

// Enregistre un article dans la bd
export async function createArticle(req, res, next) {
  const { title, content } = req.body;

  const post = new Article({
    title: title,
    content: content
  });

  try {
    const result = await post.save();
    res.location('/article/' + result._id);
    res.status(201).json(result);
  } catch (err) {
    console.log('err==>', err);
    next(err);
  }
}

// Supprime un article grâce à son id
export async function deleteArticle(req, res, next) {
  const postId = req.params.postId;
  try {
    await Article.findByIdAndDelete(postId);
    res.status(204).send();
  } catch (err) {
    next(err);
  }
}

// Modifie un article grâce à son id
export async function updateArticle(req, res, next) {
  const postId = req.params.postId;
  const { title, content } = req.body;

  try {
    const post = await Article.findById(postId);
    if (!post) {
      const error = new Error('L\'article n\'existe pas.');
      error.statusCode = 404;
      throw error;
    }
    post.title = title;
    post.content = content;
    const result = await post.save();
    res.status(200).json(result);
  } catch (err) {
    next(err);
  }
}
