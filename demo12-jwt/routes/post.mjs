import express from 'express';
import {
  getPosts,
  getPost,
  createPost,
  deletePost,
  updatePost
} from '../controllers/postsController.mjs';
import isAuth from "../middleware/isAuth.mjs";

const router = express.Router();

// /posts => GET
router.get('/posts', getPosts);

// /post/postId => GET
router.get('/post/:postId', getPost);

// /post => POST
router.post('/post/', isAuth, createPost);

// /post/postId => DELETE
router.delete('/post/:postId', isAuth, deletePost);

// /post/postId => PUT
router.put('/post/:postId', isAuth, updatePost);

export default router;

