"use strict";

import express from 'express';
import { login, createUser } from '../controllers/authController.mjs'; 

const router = express.Router();

// /auth/login/ => POST
router.post("/login", login);

// /auth/signup/ => POST
router.post("/signup", createUser);

export default router;
