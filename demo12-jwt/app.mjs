"use strict";

import express from 'express';
import mongoose from 'mongoose';

const app = express();
const port = 3000;

import {
  get404,
  getErrors
} from './controllers/errorController.mjs';

// Importe les routes
import postRoutes from './routes/post.mjs';
import authRoutes from './routes/auth.mjs';

// parse application/json
// Permet de parser le corps des requêtes (body) en JSON
app.use(express.json());  

// Utilisation des routes en tant que middleware
app.use(postRoutes);
app.use(authRoutes);

// Gestion erreur 404
app.use(get404);

// Gestion des erreurs
app.use(getErrors);

mongoose
  .connect('mongodb://127.0.0.1:27017/blogue')
  .then(() => {
    app.listen(port, () => {
      console.log(`Le serveur écoute sur http://localhost:${port}`);
    });
  })
  .catch(err => console.log(err));

