import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();

	/** Vérifie si la requête a un token JWT valide */
const isAuth = (req, res, next) => {
  // Récupère le jeton depuis l'en-tête Authorization de la requête
  const authHeader = req.get('Authorization');

  // Vérifie si l'en-tête Authorization est présent
  if (!authHeader) {
    return res.status(401).json({ error: 'Non authentifié.' });
  }

  // Récupère le jeton JWT
  const token = authHeader.split(' ')[1];
  let decodedToken;

  try {
    // Vérifie le jeton et récupére les données associées
    decodedToken = jwt.verify(token, process.env.SECRET_JWT);
    // Ajoute les données associées à l'objet de requête pour utilisation ultérieure
    req.user = decodedToken;
    console.log('req.user', req.user)
    next();
  } catch (err) {
    err.statusCode = 401;
    return next(err);
  }
};

export default isAuth;
