import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import dotenv from "dotenv";
dotenv.config();

import User from "../models/user.mjs";

export async function createUser(req, res, next) {
  const { name, email, password } = req.body;

  console.log('email', email)

  try {
    // Encryption du mot de passe
    const hashedPassword = await bcrypt.hash(password, 12);
    // Création d'un nouvel utilisateur
    const user = new User({
      email: email,
      name: name,
      password: hashedPassword,
    });

    await user.save();
    res.status(201).json({user, message: "Utilisateur créé !" });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

export async function login(req, res, next) {
  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email: email });
    if (!user) {
      const error = new Error("Courriel ou mot de passe invalide");
      error.statusCode = 401;
      throw error;
    }

    // Vérifie si le mot de passe est valide
    const isEqual = await bcrypt.compare(password, user.password);
    if (!isEqual) {
      const error = new Error("Courriel ou mot de passe invalide");
      error.statusCode = 401;
      throw error;
    }

    // Création d'un jeton JWT
    const token = jwt.sign(
      {
        email: user.email,
        name: user.name,
        id: user.id,
      },
      process.env.SECRET_JWT,
      { expiresIn: "1h" }
    );

    res.status(200).json({ token: token });
  } catch (err) {
    next(err);
  }
};

