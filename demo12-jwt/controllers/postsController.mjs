// Récupère le modèle Post
import Post from '../models/post.mjs';

export async function getPosts(req, res, next) {
  try {
    const posts = await Post.find();
    res.json(posts);
  } catch (err) {
    next(err);
  }
};

// Récupère un article grâce à son id
export async function getPost(req, res, next) {
  const postId = req.params.postId;
  try {
    const post = await Post.findById(postId);
    if (!post) {
      const error = new Error('L\'article n\'existe pas.');
      error.statusCode = 404;
      throw error;
    }
    res.status(200).json(post);
  } catch (err) {
    next(err);
  }
};

// Enregistre un article dans la bd
export async function createPost(req, res, next) {
  const { title, content } = req.body;

  console.log('req.user.id', req.user.id)

  const post = new Post({
    title: title,
    content: content,
    user: req.user.id
  });

  try {
    const result = await post.save();
    res.location('/article/' + result._id);
    res.status(201).json(result);
  } catch (err) {
    next(err);
  }
};

// Supprime un article grâce à son id
export async function deletePost(req, res, next) {
  const postId = req.params.postId;
  try {
    await Post.findByIdAndDelete(postId);
    res.status(204).send();
  } catch (err) {
    next(err);
  }
};

// Modifie un article grâce à son id
export async function updatePost(req, res, next) {
  const postId = req.params.postId;
  const { title, content } = req.body;
  try {
    const post = await Post.findById(postId);
    if (!post) {
      const error = new Error('L\'article n\'existe pas.');
      error.statusCode = 404;
      throw error;
    }
    post.title = title;
    post.content = content;
    const result = await post.save();
    res.status(200).json(result);
  } catch (err) {
    next(err);
  }
};