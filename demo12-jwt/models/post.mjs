import mongoose from "mongoose";
const { model, Schema } = mongoose;

const postSchema = new Schema(
  {
    title: {
      type: String,
      required: [true, "le titre est requis"]
    },
    content: {
      type: String,
      required: [true, "le contenu est requis"]
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: [true, "l'utilisateur est requis"]
    }
  },
  { timestamps: true }
);

const Post = model('Post', postSchema);
export default Post;