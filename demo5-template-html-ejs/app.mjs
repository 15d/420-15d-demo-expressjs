"use strict";

import path from 'path';
import express from 'express'
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const app = express();
const port = 3001;

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Configuration pour template EJS
app.set('view engine', 'ejs');
// Déclarer le dossier views qui contient les templates
app.set('views', 'views');

// Déclarer le dossier public qui contient les fichiers statiques
app.use(express.static(path.join(__dirname, 'public')));

// N'ayant pas encore de base de données, on défini quelques titre d'articles
const posts = [{title: 'Un article'}, {title: 'Un 2ème article'}];


// Déclaration d'un parser pour analyser "le corps (body)" d'une requête entrante avec POST  
// Permet donc d'analyser
app.use(express.urlencoded({
  extended: false
}));

app.get('/article/new', (req, res, next) => {
  res.render('add-post', {
    pageTitle: 'Ajouter un article'
  });
});

app.post('/article/new', (req, res, next) => {
    console.log(req.body);
    // Ajout du titre dans posts
    posts.push(req.body)
    res.redirect('/');
});

app.get('/', (req, res, next) => {
  res.render('index', {
    posts: posts,
    pageTitle: 'Accueil'
  });
});

app.use((req, res, next) => {
  res.status(404).render('404',
   { pageTitle: 'Page introuvable !' }
   );
});

app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});

