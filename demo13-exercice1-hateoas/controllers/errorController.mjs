export const get404 = (req, res) => {
  res.status(404).json({ pageTitle: 'Page introuvable !' });
};

