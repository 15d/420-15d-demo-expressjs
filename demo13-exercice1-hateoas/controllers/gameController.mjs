import dotenv from 'dotenv';
dotenv.config();

const url_base = process.env.URL + ":" + process.env.PORT;

let data;

const calculateDamage = (min, max) => {
  return Math.max(Math.floor(Math.random() * max) + 1, min);
};

const estVaincu = () => {
  if (data.monstreSante <= 0 || this.joueurSante <= 0) {
    return true;
  } 
  return false;
};

export const gameInit = (req, res, next) => {
  data = {
    joueurSante: 100,
    monstreSante: 100,
  }
  // Renvoyer la réponse avec les liens HATEOAS : self et attaque 
  // ...
  // ...
  // ...
};

export const attaque = (req, res, next) => {
  // Attaque du joueur sur le monstre
  let damage = calculateDamage(3, 10);
  data.monstreSante -= damage;

  // Attaque du monstre
  damage = calculateDamage(3, 10);
  data.joueurSante -= damage;

  // Teste si l'un des deux joueurs est vaincu !
  // Si oui, le jeu est terminé, on peut seulement
  // retourner vers / pour initialiser le jeu
  if (estVaincu()) {
    // Renvoyer la réponse avec les liens HATEOAS : self et init 
    // ...
    // ...
    // ...
  }

  // Renvoyer la réponse avec les liens HATEOAS pour l'attaque 
  // et la santé si la santé du joueur est <= 50
  // ...
  // ...
  // ...
};


export const sante = (req, res, next) => {
  // On se refait une santé
  data.joueurSante += 10;

  // Attaque du monstre
  let damage = calculateDamage(3, 10);
  data.joueurSante -= damage;

  // Renvoyer la réponse avec les liens HATEOAS : self et attaque 
  // ...
  // ...
  // ...
};
