"use strict";

import express from 'express';

const router = express.Router();

import { gameInit, attaque, sante } from '../controllers/gameController.mjs';

// / => GET
router.get('/', gameInit);

// /attaque => GET
router.get('/attaque', attaque);

// /sante => GET
router.get('/sante', sante);


export default router;
