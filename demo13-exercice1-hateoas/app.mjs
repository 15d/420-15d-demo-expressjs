"use strict";

import express from 'express'
import hateoasLinker from 'express-hateoas-links';
import dotenv from 'dotenv';

import {
  get404
} from './controllers/errorController.mjs';
// Importe les routes
import gameRoutes from './routes/game.mjs';

dotenv.config();
const app = express();

// Déclaration d'un parser pour analyser "le corps (body)" d'une 'requête entrante avec POST  
// Permet donc d'analyser
app.use(express.json()); 

// remplace le res.json standard avec la nouvelle version
// qui prend en charge les liens HATEOAS
app.use(hateoasLinker); 

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});



// Utilisation des routes en tant que middleware
app.use(gameRoutes);

app.use(get404);

// Gestion des erreurs
// "Attrappe" les erreurs envoyé par "throw"
app.use(function (err, req, res, next) {
  console.log('err', err);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).json({ message: err.message, statusCode: err.statusCode });
});


app.listen(process.env.PORT, () => {
  console.log('Node.js est à l\'écoute sur le port %s ', process.env.PORT);
})

