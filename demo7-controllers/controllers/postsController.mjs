"use strict";

import { posts } from './adminController.mjs';

export function getPosts(req, res) {
  res.render('index', {
    posts: posts,
    pageTitle: 'Accueil'
  });
};

