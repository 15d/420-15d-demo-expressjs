"use strict";

// N'ayant pas encore de base de données, on défini quelques titre d'articles
const posts = [{title: 'Un article'}, {title: 'Un 2ème article'}];
export { posts };

// Controller pour la vue du formulaire pour ajouter un article
export function getAddPost(req, res) {
  res.render('add-post', {
    pageTitle: 'Ajouter un article'
  });
};

// Controller pour l'ajout d'un article quand le formulaire est soumis
// Récupère les données du formulaire avec req.body
export function createPost(req, res) {
    console.log(req.body);
      // Ajout du titre dans posts
      posts.push(req.body);
      res.redirect('/');
}
