"use strict";

import express from 'express'
import * as postsController from '../controllers/postsController.mjs';

const router = express.Router();

// / => GET
router.get('/', postsController.getPosts);

export default router;



