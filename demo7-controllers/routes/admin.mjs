"use strict";

import express from 'express'
import * as adminController from '../controllers/adminController.mjs';

const router = express.Router();

// Vue du formulaire pour ajouter un article
// /admin/add-post => GET
router.get('/add-post', adminController.getAddPost);

// Ajout d'un article quand le formulaire est soumis
// /admin/add-post => POST
router.post('/add-post', adminController.createPost);

// Export des routes pour utilisation dans app.js
export default router;
