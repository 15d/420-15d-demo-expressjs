import dotenv from 'dotenv';
dotenv.config();

// Récupère le modèle Post
import Post from '../models/post.mjs';

const url_base = process.env.URL + ":" + process.env.PORT;

export async function getPosts(req, res, next) {
  try {
    const posts = await Post.find();
    // Ajoute des liens pour les actions possibles sur chaque article
    const postLinks = posts.map(post => {
      return {
        ...post.toJSON(),
        links: [
          { rel: 'self', method:'GET', href: `${url_base}/post/${post._id}` },
          { rel: 'delete', method:'DELETE', href: `${url_base}/post/${post._id}` },
          { rel: 'update', method:'PUT', href: `${url_base}/post/${post._id}` }
        ]
      };
    });
    res.json(postLinks);
  } catch (err) {
    next(err);
  }
};

// Récupère un article grâce à son id
export async function getPost(req, res, next) {
  const postId = req.params.postId;
  try {
    const post = await Post.findById(postId);
    if (!post) {
      const error = new Error('L\'article n\'existe pas.');
      error.statusCode = 404;
      throw error;
    }
    // Ajoute des liens pour les actions possibles sur l'article
    const postLinks = {
      ...post.toJSON(),
      links: [
        { rel: 'self', method:'GET', href: `${url_base}/post/${post._id}` },
        { rel: 'delete', method:'DELETE', href: `${url_base}/post/${post._id}` },
        { rel: 'update', method:'PUT', href: `${url_base}/post/${post._id}` }
      ]
    };
    res.status(200).json(postLinks);
  } catch (err) {
    next(err);
  }
};

// Enregistre un article dans la bd
export async function createPost(req, res, next) {
  const { title, content } = req.body;

  const post = new Post({
    title: title,
    content: content
  });

  try {
    const result = await post.save();
    res.location('/article/' + result._id);
    res.status(201).json(result);
  } catch (err) {
    next(err);
  }
};

// Supprime un article grâce à son id
export async function deletePost(req, res, next) {
  const postId = req.params.postId;
  try {
    await Post.findByIdAndRemove(postId);
    res.status(204).send();
  } catch (err) {
    next(err);
  }
};

// Modifie un article grâce à son id
export async function updatePost(req, res, next) {
  const postId = req.params.postId;
  const { title, content } = req.body;
  try {
    const post = await Post.findById(postId);
    if (!post) {
      const error = new Error('L\'article n\'existe pas.');
      error.statusCode = 404;
      throw error;
    }
    post.title = title;
    post.content = content;
    const result = await post.save();
    res.status(200).json(result);
  } catch (err) {
    next(err);
  }
};