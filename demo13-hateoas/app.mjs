"use strict";

import express from 'express'
import mongoose from 'mongoose';
import dotenv from 'dotenv';
const app = express();
dotenv.config();
const port = process.env.PORT || 3000;

import {
  getErrors,
  get404
} from './controllers/errorController.mjs';
// Importe les routes
import postRoutes from './routes/post.mjs';

// parse application/json
// Permet de parser le corps des requêtes (body) en JSON
app.use(express.json());  

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

// Utilisation des routes en tant que middleware
app.use(postRoutes);

// Gestion erreur 404
app.use(get404);

// Gestion des erreurs
app.use(getErrors);

mongoose
  .connect(process.env.MONGODB)
  .then(() => {
    app.listen(port, () => {
      console.log(`Le serveur écoute sur http://localhost:${port}`);
    });
  })
  .catch(err => console.log(err));

