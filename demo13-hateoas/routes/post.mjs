import express from 'express'
import {
  getPosts,
  getPost,
  createPost,
  deletePost,
  updatePost
} from '../controllers/postsController.mjs';

const router = express.Router();

// /posts => GET
router.get('/posts', getPosts);

// /post/postId => GET
router.get('/post/:postId', getPost);

// /post => POST
router.post('/post/', createPost);

// /post/postId => DELETE
router.delete('/post/:postId', deletePost);

// /post/postId => PUT
router.put('/post/:postId', updatePost);

export default router;

