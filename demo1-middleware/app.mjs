"use strict";

import express from 'express';

const app = express();

const port = 3001;

app.use("/article", (req, res, next) => {
    console.log('Dans le premier middleware!');
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    next(); // Autorise la requête à aller au prochain middleware
});

app.use((req, res, next) => {
    console.log('Dans un autre middleware!');
    res.send('<h1>Salut depuis Express!</h1>');
});

app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});
